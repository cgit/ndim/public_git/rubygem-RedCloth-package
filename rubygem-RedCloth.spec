# Generated from RedCloth-4.1.9.gem by gem2rpm -*- rpm-spec -*-
%define ruby_sitelib %(ruby -rrbconfig -e "puts Config::CONFIG['sitelibdir']")
%define gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%define gemname RedCloth
%define geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: RedCloth-4.1.9 - Textile parser for Ruby. http://redcloth.org/
Name: rubygem-%{gemname}
Version: 4.1.9
Release: 1%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://redcloth.org
Source0: http://gems.rubyforge.org/gems/%{gemname}-%{version}.gem
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: rubygems
BuildRequires: rubygems
Provides: rubygem(%{gemname}) = %{version}

%description
RedCloth-4.1.9 - Textile parser for Ruby. http://redcloth.org/


%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force --rdoc %{SOURCE0}
mkdir -p %{buildroot}/%{_bindir}
mv %{buildroot}%{gemdir}/bin/* %{buildroot}/%{_bindir}
rmdir %{buildroot}%{gemdir}/bin
find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%{_bindir}/redcloth
%{gemdir}/gems/%{gemname}-%{version}/
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/CHANGELOG
%doc %{geminstdir}/lib/case_sensitive_require/RedCloth.rb
%doc %{geminstdir}/lib/redcloth/erb_extension.rb
%doc %{geminstdir}/lib/redcloth/formatters/base.rb
%doc %{geminstdir}/lib/redcloth/formatters/html.rb
%doc %{geminstdir}/lib/redcloth/formatters/latex.rb
%doc %{geminstdir}/lib/redcloth/textile_doc.rb
%doc %{geminstdir}/lib/redcloth/version.rb
%doc %{geminstdir}/lib/redcloth.rb
%doc %{geminstdir}/README
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Wed Apr 01 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 4.1.9-1
- Initial package
